# frozen_string_literal: true

require_relative 'repository'
require_relative 'constants'

Repository.new(Constants::Paths::DB)

require_relative 'pubsub_service'
require_relative 'handler'
require_relative 'misc_handler'
require_relative 'group_handler'
require_relative 'event_handler'
require_relative 'chat_service'
require_relative 'config'

class App
  def initialize(pubsub_service)
    @pubsub_service = pubsub_service
    @exit = false
    Signal.trap('INT') do
      puts 'Exiting gracefully'
      @exit = true
    end
  end

  def start
    loop do
      exit if @exit
      @pubsub_service.pull
      sleep 5
    end
  end
end

chat_service = ChatService.new

App.new(
  PubsubService.new(
    Config.new(Constants::Paths::Config).get_subscription,
    Handler.new(
      misc_handler: MiscHandler.new(chat_service),
      group_handler: GroupHandler.new(chat_service),
      event_handler: EventHandler.new(chat_service)
    )
  )
).start
