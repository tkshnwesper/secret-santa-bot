# frozen_string_literal: true

class MiscHandler
  def initialize(chat_service)
    @chat_service = chat_service
  end

  def send_usage_instructions(message)
    @chat_service.send_message(message.space_name, Constants::Strings::Usage, message.thread_name)
  end

  def send_error_message(message, error)
    @chat_service.send_message(message.space_name, error.message, message.thread_name)
  end
end
