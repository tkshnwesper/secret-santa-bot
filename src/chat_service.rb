# frozen_string_literal: true

require 'google/apis/chat_v1'

require_relative 'authorizer'

class ChatService
  Chat = Google::Apis::ChatV1
  Scope = 'https://www.googleapis.com/auth/chat.bot'

  def initialize
    @chat = Chat::HangoutsChatService.new
    @chat.authorization = Authorizer.new(ChatService::Scope).instance
  end

  def send_message(space, text, thread_name = nil)
    message = Chat::Message.new
    thread = Chat::Thread.new
    thread.name = thread_name
    message.text = text
    message.thread = thread
    @chat.create_space_message(space, message, :thread_key => thread_name)
  end
end
