# frozen_string_literal: true

class HandlerUtils
  def self.get_or_create_user(user_name, user_display_name, space_name)
    user = Models::User.first(name: user_name)
    if user.nil?
      user = Models::User.create(
        space: space_name,
        name: user_name,
        display_name: user_display_name
      )
    end
    user
  end

  def self.extract_group_name(text)
    /\S+$/.match(text).to_s
  end

  def self.get_or_create_group(group_name)
    group = Models::Group.first(name: group_name)
    if group.nil?
      group = Models::Group.create(
        name: group_name,
      )
    end
    group
  end
end
