# frozen_string_literal: true

require 'sequel'

class Repository
  attr_reader :instance

  def initialize(path)
    @instance = Sequel.connect("sqlite://#{path}")
  end
end
