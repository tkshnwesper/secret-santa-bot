# frozen_string_literal: true

require 'googleauth'
require_relative 'constants'

require_relative 'config'

class Authorizer
  attr_reader :instance

  def initialize(scope)
    @instance = Google::Auth::ServiceAccountCredentials.make_creds(
      json_key_io: File.open(Config.new(Constants::Paths::Config).get_cred_file),
      scope: scope
    )
  end
end
