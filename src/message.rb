# frozen_string_literal: true

require 'json'

class Message
  attr_reader :message_text, :user_name, :user_display_name, :space_name, :thread_name

  def initialize(raw_message)
    message = JSON.parse(raw_message)
    @message_text = message['message']['text']
    @user_name = message['user']['name']
    @user_display_name = message['user']['displayName']
    @space_name = message['space']['name']
    @thread_name = message['message']['thread']['name']
  end
end
