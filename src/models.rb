# frozen_string_literal: true

require 'sequel'

module Models
  class User < Sequel::Model
    many_to_many :groups
  end

  class Group < Sequel::Model
    many_to_many :users
    many_to_one :admin, key: :admin_id, class: User
  end

  class Santa < Sequel::Model
    one_to_one :friend, class: self, key: :friend_id
  end
end
