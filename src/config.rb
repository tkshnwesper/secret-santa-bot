# frozen_string_literal: true

require 'yaml'

class Config
  def initialize(file_path)
    @config = YAML.load_file(file_path)
  end

  def get_cred_file
    @config['cred_file']
  end

  def get_subscription
    @config['subscription']
  end
end
