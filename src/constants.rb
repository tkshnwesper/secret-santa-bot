module Constants
  module Strings
    Usage = <<-MSG
Usage instructions:

`start event <group-name>`
Starts the secret santa event by sending out santees to all the respective group members.

`register <group-name>`
Registers you with the provided group name.

`deregister <group-name>`
Unregisters you from the provided group.

`create group <group-name>`
Creates a group.

`delete group <group-name>`
Deletes the group.

`show users <group-name>`
Displays a list of users registered to the group.

`help`
Shows usage instructions.
    MSG
  end

  module Paths
    DB = 'app.db'
    Config = 'config.yaml'
  end
end