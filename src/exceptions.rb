# frozen_string_literal: true

class BaseError < StandardError
end

class CommandNotFoundException < BaseError
  def initialize(command)
    super "\"#{command}\" does not match any command.\n#{Constants::Strings::Usage}"
  end
end

class GroupAlreadyExistsException < BaseError
  def initialize(group_name)
    super "Group *#{group_name}* already exists"
  end
end

class UserAlreadyBelongsToGroupException < BaseError
  def initialize(user_display_name, group_name)
    super "User *#{user_display_name}* already belongs to group *#{group_name}*"
  end
end

class GroupDoesNotExistException < BaseError
  def initialize(group_name)
    super "Group *#{group_name}* does not exist"
  end
end

class UserDoesNotExistException < BaseError
  def initialize(user_display_name, group_name)
    super "User *#{user_display_name}* does not exist in group *#{group_name}"
  end
end

class AdminCannotDeregisterException < BaseError
  def initialize(group_name)
    super "Use `delete group #{group_name}` instead"
  end
end

class NonAdminCannotDeleteGroupException < BaseError
  def initialize
    super 'Only an admin of the group can delete the group'
  end
end
