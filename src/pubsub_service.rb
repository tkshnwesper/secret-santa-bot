# frozen_string_literal: true

require 'google/apis/pubsub_v1'

require_relative 'authorizer'
require_relative 'message'

class PubsubService
  Pubsub = Google::Apis::PubsubV1
  Scope = 'https://www.googleapis.com/auth/pubsub'

  def initialize(subscription, handler)
    @subscription = subscription
    @authorization = Authorizer.new(PubsubService::Scope).instance
    @handler = handler
    @pubsub = Pubsub::PubsubService.new
    @pubsub.authorization = @authorization
  end

  def pull
    response = @pubsub.pull_subscription(
      @subscription,
      Pubsub::PullRequest.new(max_messages: 5, return_immediately: true)
    )
    unless response.received_messages.nil?
      response.received_messages.each do |received_message|
        message = Message.new(received_message.message.data)
        @handler.handle_message(message)
      end
      ack_ids = response.received_messages.map(&:ack_id)
      @pubsub.acknowledge_subscription(@subscription, Pubsub::AcknowledgeRequest.new(ack_ids: ack_ids))
    end
  end
end
