# frozen_string_literal: true

require 'date'

require_relative 'models'
require_relative 'handler_utils'
require_relative 'exceptions'

class SantaPair
  def initialize(santa, friend)
    @santa = santa
    @friend = friend
  end

  def send_message(chat_service)
    chat_service.send_message(
      @santa.space,
      "Your secret friend is *#{@friend.display_name}*"
    )
  end

  def save_pair(group)
    Models::Santa.create(
      santa_id: @santa[:id],
      friend_id: @friend[:id],
      group_id: group[:id]
    )
  end
end

class EventHandler
  def initialize(chat_service)
    @chat_service = chat_service
  end

  def start_event(message)
    group_name = HandlerUtils.extract_group_name(message.message_text)
    group = Models::Group.first(name: group_name)
    raise GroupDoesNotExistException, group_name if group.nil?

    Models::Santa.where(group_id: group[:id]).delete
    users = group.users.map(&:clone)
    pairs = generate_pairs(users)
    pairs.each do |pair|
      pair.save_pair(group)
      pair.send_message(@chat_service)
    end
  end

  private

  def generate_pairs(users)
    first = users.shuffle!.pop
    prev = first
    curr = prev
    pairs = []
    until users.empty?
      curr = users.shuffle!.pop
      pairs << SantaPair.new(prev, curr)
      prev = curr
    end
    pairs << SantaPair.new(curr, first)
  end
end
