# frozen_string_literal: true

require_relative 'models'
require_relative 'handler_utils'
require_relative 'exceptions'

class GroupHandler
  def initialize(chat_service)
    @chat_service = chat_service
  end

  def show_users(message)
    group_name = HandlerUtils.extract_group_name(message.message_text)
    group = Models::Group.first(name: group_name)
    raise GroupDoesNotExistException, group_name if group.nil?

    users = group.users.map { |user| user[:display_name] }.sort.join("\n")
    @chat_service.send_message(
      message.space_name,
      "Users in *#{group_name}*:\n#{users}",
      message.thread_name
    )
  end

  def create(message)
    user = HandlerUtils.get_or_create_user(
      message.user_name,
      message.user_display_name,
      message.space_name
    )
    group_name = HandlerUtils.extract_group_name(message.message_text)
    group = create_group(group_name, user)
    user.add_group(group)

    @chat_service.send_message(
      message.space_name,
      "Successfully created group *#{group_name}*",
      message.thread_name
    )
  end

  def delete(message)
    group_name = HandlerUtils.extract_group_name(message.message_text)
    group = Models::Group.first(name: group_name)
    raise GroupDoesNotExistException, group_name if group.nil?

    admin = Models::User
            .select_all(:users)
            .association_join(:groups)
            .first(
              Sequel[:users][:name] => message.user_name,
              Sequel[:users][:id] => Sequel[:groups][:admin_id]
            )
    raise NonAdminCannotDeleteGroupException if admin.nil?

    group.users.each do |user|
      response = "Group *#{group_name}* has been deleted"
      if user == admin
        @chat_service.send_message(
          message.space_name,
          response,
          message.thread_name
        )
      else
        @chat_service.send_message(
          user.space,
          response
        )
      end
    end
    group.remove_all_users
    group.delete
  end

  def register(message)
    user = HandlerUtils.get_or_create_user(
      message.user_name,
      message.user_display_name,
      message.space_name
    )
    group_name = HandlerUtils.extract_group_name(message.message_text)
    group = Models::Group.first(name: group_name)
    raise GroupDoesNotExistException, group_name if group.nil?

    if user.groups.include?(group)
      raise UserAlreadyBelongsToGroupException.new(
        user.display_name, group_name
      )
    end

    user.add_group(group)
    @chat_service.send_message(
      message.space_name,
      "Successfully registered to *#{group_name}*",
      message.thread_name
    )
  end

  def deregister(message)
    group_name = HandlerUtils.extract_group_name(message.message_text)
    user = Models::User
           .select_all(:users)
           .association_join(:groups)
           .first(
             Sequel[:groups][:name] => group_name,
             Sequel[:users][:name] => message.user_name
           )
    raise UserDoesNotExistException.new(message.user_display_name, group_name) if user.nil?

    group = Models::Group.first(name: group_name)
    raise AdminCannotDeregisterException, group_name if user == group.admin

    user.remove_group group
    @chat_service.send_message(
      message.space_name,
      "Successfully unregistered from *#{group_name}*",
      message.thread_name
    )
  end

  private

  def create_group(group_name, user)
    group = Models::Group.first(name: group_name)
    raise GroupAlreadyExistsException, group_name unless group.nil?

    group = Models::Group.create(
      name: group_name,
      admin_id: user[:id]
    )
  end
end
