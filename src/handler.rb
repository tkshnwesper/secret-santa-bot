# frozen_string_literal: true

require_relative 'constants'

class Handler
  def initialize(
    misc_handler: nil,
    group_handler: nil,
    event_handler: nil
  )
    @misc_handler = misc_handler
    @group_handler = group_handler
    @event_handler = event_handler
  end

  def handle_message(message)
    evaluate_command(message)
  rescue BaseError => e
    @misc_handler.send_error_message(message, e)
  end

  private

  def evaluate_command(message)
    case message.message_text
    when /start event \S+$/i
      @event_handler.start_event(message)
    when /show users \S+$/i
      @group_handler.show_users(message)
    when /deregister \S+$/i
      @group_handler.deregister(message)
    when /register \S+$/i
      @group_handler.register(message)
    when /create group \S+$/i
      @group_handler.create(message)
    when /delete group \S+$/i
      @group_handler.delete(message)
    when /help$/i
      @misc_handler.send_usage_instructions(message)
    else
      raise CommandNotFoundException, message.message_text
    end
  end
end
