Sequel.migration do
  up do
    create_table(:groups) do
      primary_key :id
      String :name, unique: true, null: false
      foreign_key :admin_id, :users
    end
  end

  down do
    drop_table(:groups)
  end
end
