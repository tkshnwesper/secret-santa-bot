Sequel.migration do
  up do
    create_table(:santas) do
      primary_key :id
      foreign_key :santa_id, :users, :null => false
      foreign_key :friend_id, :users, :null => false
      foreign_key :group_id, :groups, :null => false
    end
  end

  down do
    drop_table(:santas)
  end
end
