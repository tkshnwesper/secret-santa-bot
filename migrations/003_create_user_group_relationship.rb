Sequel.migration do
  up do
    create_join_table(user_id: :users, group_id: :groups)
  end

  down do
    drop_join_table(user_id: :users, group_id: :groups)
  end
end
