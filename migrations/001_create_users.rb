Sequel.migration do
  up do
    create_table(:users) do
      primary_key :id
      String :space, unique: true, null: false
      String :name, unique: true, null: false
      String :display_name
    end
  end

  down do
    drop_table(:users)
  end
end