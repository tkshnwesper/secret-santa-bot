# Secret santa

Secret santa over Google Chat.

## Pre-requisites

### Installing

```shell
bundle install
```

### Config file

Create a `config.yaml` as follows:

```yaml
cred_file: <path-to-cred-file>
subscription: <subscription-url>
```

### DB Migrations

Run DB migrations with the following command:

```shell
rake db:migrate
```

## Running

```shell
rake run
```

### Running tests

```shell
rake test
```
