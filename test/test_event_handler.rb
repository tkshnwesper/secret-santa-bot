# frozen_string_literal: true

require 'test/unit'

require_relative 'utils'
require_relative '../src/handler'
require_relative '../src/event_handler'
require_relative '../src/misc_handler'

class TestEventHandler < Test::Unit::TestCase
  def setup
    Utils.migrate_up
  end

  def teardown
    Utils.migrate_down
  end

  def test_starts_event_successfully
    chat_service_stub = ChatServiceStub.new
    group_name = 'xyz'
    user_thread = 't'
    user_space = 's'
    user_name = 'u'
    user_display_name = 'saurabh'
    admin_thread = 'at'
    admin_space = 'as'
    admin_display_name = 'admin'
    handler = Handler.new(
      event_handler: EventHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        thread: admin_thread,
        space: admin_space,
        display_name: admin_display_name
      )
    )
    handler.handle_message(
      create_message(
        text: "register #{group_name}",
        user_name: user_name,
        display_name: user_display_name,
        thread: user_thread,
        space: user_space
      )
    )
    handler.handle_message(
      create_message(
        text: "start event #{group_name}",
        thread: admin_thread,
        space: admin_space,
        display_name: admin_display_name
      )
    )
    assert_not_nil(Models::Santa.first(santa_id: 1, friend_id: 2))
    assert_not_nil(Models::Santa.first(santa_id: 2, friend_id: 1))
    assert(chat_service_stub.message_list.include?(
             thread: nil,
             space: admin_space,
             message: "Your secret friend is *#{user_display_name}*"
           ))
    assert(chat_service_stub.message_list.include?(
             thread: nil,
             space: user_space,
             message: "Your secret friend is *#{admin_display_name}*"
           ))
  end

  def test_start_event_wipes_historic_santa_data_before_starting
    chat_service_stub = ChatServiceStub.new
    group_name = 'lalala'
    handler = Handler.new(
      event_handler: EventHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}"
      )
    )
    handler.handle_message(
      create_message(
        text: "start event #{group_name}"
      )
    )
    assert_equal(1, Models::Santa.count)
    handler.handle_message(
      create_message(
        text: "start event #{group_name}"
      )
    )
    assert_equal(1, Models::Santa.count)
  end

  def test_start_event_sends_error_when_group_does_not_exist
    chat_service_stub = ChatServiceStub.new
    group_name = 'xyz'
    handler = Handler.new(
      event_handler: EventHandler.new(chat_service_stub),
      misc_handler: MiscHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "start event #{group_name}"
      )
    )
    assert_equal("Group *#{group_name}* does not exist", chat_service_stub.message)
  end
end
