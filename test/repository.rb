# frozen_string_literal: true

require 'sequel/core'

module InMemoryRepository
  Instance = Sequel.sqlite(':memory:')
end

repository = InMemoryRepository::Instance
Sequel.extension :migration
Sequel::Migrator.run(repository, 'migrations')