# frozen_string_literal: true

require 'test/unit'

require_relative 'utils'
require_relative '../src/handler'
require_relative '../src/group_handler'

class TestGroupHandler < Test::Unit::TestCase
  def setup
    Utils.migrate_up
  end

  def teardown
    Utils.migrate_down
  end

  def test_handles_registration_successfully
    chat_service_stub = ChatServiceStub.new
    group_name = 'group-name'
    user_name = 'users/12930q8403948230948'
    display_name = 'Kitty Cat'
    handler = Handler.new(
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        display_name: 'Awesome admin',
        user_name: 'Some user',
        space: 'ss'
      )
    )
    handler.handle_message(
      create_message(
        text: "register #{group_name}",
        display_name: display_name,
        user_name: user_name
      )
    )
    group = Models::Group[1]
    user = Models::User.first(name: user_name)
    assert_equal(group_name, group[:name])
    assert_equal(user_name, user[:name])
    assert_equal(display_name, user[:display_name])
    assert(user.groups.include?(group))
    assert_equal("Successfully registered to *#{group_name}*", chat_service_stub.message)
  end

  def test_register_sends_error_if_user_already_belongs_to_group
    chat_service_stub = ChatServiceStub.new
    group_name = 'group-name'
    display_name = 'some person'
    handler = Handler.new(
      group_handler: GroupHandler.new(chat_service_stub),
      misc_handler: MiscHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        display_name: display_name
      )
    )
    handler.handle_message(
      create_message(
        text: "register #{group_name}",
        display_name: display_name
      )
    )
    assert_equal(
      "User *#{display_name}* already belongs to group *#{group_name}*",
      chat_service_stub.message
    )
  end

  def test_creates_group_successfully
    chat_service_stub = ChatServiceStub.new
    group_name = 'test-group'
    user_id = 'users/12930q8403948230948'
    user_name = 'Kitty Cat'
    group_handler = GroupHandler.new(chat_service_stub)
    Handler.new(group_handler: group_handler)
           .handle_message(
             create_message(
               text: "create group #{group_name}",
               display_name: user_name,
               user_name: user_id
             )
           )
    group = Models::Group[1]
    user = Models::User[1]
    assert_equal(group_name, group[:name])
    assert_equal(user_id, user[:name])
    assert_equal(user_name, user[:display_name])
    assert_equal(user[:id], group[:admin_id])
    assert(user.groups.include?(group))
    assert_equal("Successfully created group *#{group_name}*", chat_service_stub.message)
  end

  def test_sends_error_when_trying_to_create_existing_group
    chat_service_stub = ChatServiceStub.new
    group_name = 'test-group'
    user_id = 'users/12930q8403948230948'
    user_name = 'Kitty Cat'
    group_handler = GroupHandler.new(chat_service_stub)
    handler = Handler.new(
      group_handler: group_handler,
      misc_handler: MiscHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        display_name: user_name,
        user_name: user_id
      )
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        display_name: user_name,
        user_name: user_id
      )
    )
    assert_equal("Group *#{group_name}* already exists", chat_service_stub.message)
  end

  def test_handles_deregistration_successfully
    group_name = 'group-name'
    user_name = 'users/12930q8403948230948'
    display_name = 'Kitty Cat'
    chat_service_stub = ChatServiceStub.new
    group_handler = GroupHandler.new(chat_service_stub)
    handler = Handler.new(
      group_handler: group_handler
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        display_name: 'dn1',
        user_name: 'un1',
        space: 'sp1'
      )
    )
    handler.handle_message(
      create_message(
        text: "register #{group_name}",
        display_name: display_name,
        user_name: user_name
      )
    )

    group = Models::Group[1]
    user = Models::User[name: user_name]
    assert(user.groups.include?(group))

    handler.handle_message(
      create_message(
        text: "deregister #{group_name}",
        display_name: display_name,
        user_name: user_name
      )
    )

    group = Models::Group[1]
    user = Models::User[name: user_name]
    assert_equal(group_name, group[:name])
    assert_equal(user_name, user[:name])
    assert_equal(display_name, user[:display_name])
    assert(!user.groups.include?(group))
    assert_equal("Successfully unregistered from *#{group_name}*", chat_service_stub.message)
  end

  def test_shows_users_in_group_successfully
    group_name = 'group-name'
    user_name_one = 'users/12930q8403948230948'
    user_name_two = 'users/12930q8403948230923'
    display_name_one = 'Kitty Cat'
    display_name_two = 'Woof woof'
    chat_service_stub = ChatServiceStub.new
    handler = Handler.new(
      misc_handler: MiscHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        display_name: display_name_one,
        user_name: user_name_one,
        space: 'spaces/122394038123'
      )
    )
    handler.handle_message(
      create_message(
        text: "register #{group_name}",
        display_name: display_name_two,
        user_name: user_name_two,
        space: 'spaces/1223940312321'
      )
    )
    handler.handle_message(
      create_message(
        text: "show users #{group_name}",
        display_name: display_name_two,
        user_name: user_name_two
      )
    )
    assert_equal(
      "Users in *#{group_name}*:\n#{display_name_one}\n#{display_name_two}",
      chat_service_stub.message
    )
  end

  def test_show_users_sends_error_if_group_does_not_exist
    chat_service_stub = ChatServiceStub.new
    group_name = 'xyz'
    handler = Handler.new(
      misc_handler: MiscHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "show users #{group_name}"
      )
    )
    assert_equal("Group *#{group_name}* does not exist", chat_service_stub.message)
  end

  def test_register_sends_error_if_group_does_not_exist
    chat_service_stub = ChatServiceStub.new
    group_name = 'xyz'
    handler = Handler.new(
      misc_handler: MiscHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "register #{group_name}"
      )
    )
    assert_equal("Group *#{group_name}* does not exist", chat_service_stub.message)
  end

  def test_deregister_sends_error_if_user_does_not_exist
    chat_service_stub = ChatServiceStub.new
    group_name = 'xyz'
    display_name = 'saurabh'
    handler = Handler.new(
      misc_handler: MiscHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "deregister #{group_name}",
        display_name: display_name
      )
    )
    assert_equal(
      "User *#{display_name}* does not exist in group *#{group_name}",
      chat_service_stub.message
    )
  end

  def test_admin_cannot_deregister
    chat_service_stub = ChatServiceStub.new
    group_name = 'xyz'
    handler = Handler.new(
      misc_handler: MiscHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}"
      )
    )
    handler.handle_message(
      create_message(
        text: "deregister #{group_name}"
      )
    )
    assert_equal(
      "Use `delete group #{group_name}` instead",
      chat_service_stub.message
    )
  end

  def test_delete_group_successfully
    chat_service_stub = ChatServiceStub.new
    group_name = 'xyz'
    thread = 't'
    space = 's'
    admin_thread = 'at'
    admin_space = 'as'
    handler = Handler.new(
      misc_handler: MiscHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        thread: admin_thread,
        space: admin_space
      )
    )
    handler.handle_message(
      create_message(
        text: "register #{group_name}",
        user_name: 'x',
        thread: thread,
        space: space
      )
    )
    chat_service_stub.message_list.clear
    handler.handle_message(
      create_message(
        text: "delete group #{group_name}",
        thread: admin_thread,
        space: admin_space
      )
    )
    assert(chat_service_stub.message_list.include?(
             message: "Group *#{group_name}* has been deleted",
             space: space,
             thread: nil
           ))
    assert(chat_service_stub.message_list.include?(
             message: "Group *#{group_name}* has been deleted",
             space: admin_space,
             thread: admin_thread
           ))
    assert_equal(2, chat_service_stub.message_list.length)
    assert_equal(0, Models::Group.count)
  end

  def test_non_admin_cannot_delete_group
    chat_service_stub = ChatServiceStub.new
    group_name = 'xyz'
    thread = 't'
    space = 's'
    user_name = 'u'
    admin_thread = 'at'
    admin_space = 'as'
    handler = Handler.new(
      misc_handler: MiscHandler.new(chat_service_stub),
      group_handler: GroupHandler.new(chat_service_stub)
    )
    handler.handle_message(
      create_message(
        text: "create group #{group_name}",
        thread: admin_thread,
        space: admin_space
      )
    )
    handler.handle_message(
      create_message(
        text: "register #{group_name}",
        user_name: user_name,
        thread: thread,
        space: space
      )
    )
    handler.handle_message(
      create_message(
        text: "delete group #{group_name}",
        thread: thread,
        space: space,
        user_name: user_name
      )
    )
    assert_equal(
      'Only an admin of the group can delete the group',
      chat_service_stub.message
    )
    assert_equal(2, Models::Group[1].users.length)
  end
end
