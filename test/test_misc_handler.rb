# frozen_string_literal: true

require 'test/unit'

require_relative 'utils'
require_relative '../src/handler'
require_relative '../src/misc_handler'
require_relative '../src/models'
require_relative '../src/constants'

class TestMiscHandler < Test::Unit::TestCase
  def test_informs_user_of_invalid_command
    stub = ChatServiceStub.new
    space_name = 'spaces/1223940380958'
    thread_name = 'thread-name'
    Handler.new(misc_handler: MiscHandler.new(stub)).handle_message(
      create_message(space: space_name, text: 'some invalid message')
    )
    assert(stub.message.include?('"some invalid message" does not match any command.'))
    assert_equal(stub.space, space_name)
    assert_equal(stub.thread, thread_name)
  end

  def test_shows_help_text_successfully
    stub = ChatServiceStub.new
    space_name = '123'
    Handler.new(misc_handler: MiscHandler.new(stub)).handle_message(
      create_message(space: space_name, text: 'help')
    )
    assert_equal(stub.message, Constants::Strings::Usage)
    assert_equal(stub.space, space_name)
  end
end
