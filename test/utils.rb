# frozen_string_literal: true

require 'json'
require 'sequel/core'

require_relative 'repository'
require_relative '../src/message'

Sequel.extension :migration

module Utils
  def self.migrate_up
    Sequel::Migrator.run(InMemoryRepository::Instance, 'migrations')
  end

  def self.migrate_down
    Sequel::Migrator.run(InMemoryRepository::Instance, 'migrations', target: 0)
  end
end

class ChatServiceStub
  attr_reader :message, :space, :thread, :message_list

  def initialize
    @message_list = []
  end

  def send_message(space, message, thread = nil)
    @message_list << { message: message, space: space, thread: thread }
    @space = space
    @message = message
    @thread = thread
  end
end

def create_message(
  thread: 'thread-name',
  space: 'space-name',
  text:,
  display_name: 'display_name',
  user_name: 'user_name'
)
  Message.new(
    {
      'message' => {
        'text' => text,
        'thread' => {
          'name' => thread
        }
      },
      'user' => {
        'name' => user_name,
        'displayName' => display_name
      },
      'space' => {
        'name' => space
      }
    }.to_json
  )
end
