require_relative '../src/config'
require 'test/unit'

class TestConfig < Test::Unit::TestCase
  TestConfigPath = "test/data/test_config.yaml"

  def test_gets_cred_file_location_successfully
    assert_equal(Config.new(TestConfigPath).get_cred_file, 'xyz')
  end
  
  def test_gets_subscription_successfully
    assert_equal(Config.new(TestConfigPath).get_subscription, 'sub')
  end
end